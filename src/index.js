import React from 'react';
import { render } from 'react-dom';
// import StorePicker from './components/StorePicker'; now is in Router
// import App from './components/App';

import Router from './components/Router';

import "./css/style.css";

render(<Router/>, document.querySelector('#main'));
// render(<StorePicker/>, document.querySelector('#main'));

 