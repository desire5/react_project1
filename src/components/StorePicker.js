import "../css/style.css";
import React from 'react';
import { getFunName } from "../helpers";

class StorePicker extends React.Component {

	// constructor(){
	// 	super();
	// 	this.goToStore = this.goToStore.bind(this);
	// 	console.log('start');
	// }
	myInput = React.createRef();

	goToStore = (e) => {
		e.preventDefault();
		// console.log(this);  this will be if use arrow funct. if not arrow need use constructor (	this.goToStore = this.goToStore.bind(this); bind(this) )
		console.log(this.myInput.current.value); 
		const storeNmae = this.myInput.current.value;
		this.props.history.push(`/store/${storeNmae}`);
		// const storeName = document.querySelector('input').val;
	}

	render(){
		return (
			<form className="store-selector" onSubmit={this.goToStore}>
				<h2>Please enter A Store</h2>
				<input type="text" required="" placeholder="store name" 
					defaultValue={getFunName()}
					ref={this.myInput}
				/>
				<button type='submit'>Visit Store</button>
			</form>
			)	
		// return React.createElement('h1',{className:'aaa'}, 'component');
		 
	}
}
export default StorePicker;