import React from 'react';
import PropTypes from "prop-types";

const Header = (props) =>  ( 
			<header className="top">
				<h1>Catch 
					<span className="ofThe">
						<span className="of">Of</span>
						<span className="the">the</span>
					</span>
					Day 
				</h1>	
				<h3 className="tagline">
					<span>{props.tagline} {props.age}  {props.aa}</span>
				</h3>
			</header>

		);	

// class Header extends React.Component {
// 	render(){
// 		return ( 
// 			<header className="top">
// 				<h1>Catch 
// 					<span className="ofThe">
// 						<span className="of">Of</span>
// 						<span className="the">the</span>
// 					</span>
// 					Day 
// 				</h1>	
// 				<h3 className="tagline">
// 					<span>{this.props.tagline}</span>
// 				</h3>
// 			</header>

// 		);	
		
// 	}
// }

// validation of props
Header.propTypes = {
	tagline: PropTypes.string.isRequired
};
export default Header;