import React from 'react';
import Header from './Header';
import Inventory from './Inventory';
import Order from './Order'; 
import Fish from './Fish';
import samleFishes from '../sample-fishes';
import base from "../base";

class App extends React.Component {

	 state = {
		fishes: {},
		order: {},
	};

	componentDidMount(){
		const {params} = this.props.match; 
		console.log('loaded page ', params.storeId);

		const localStorageRef = localStorage.getItem(params.storeId);
		console.log('loaded local stor ', localStorageRef);

		if(localStorageRef){
			// this.state.order = JSON.parse(localStorageRef);
			this.setState({ order: JSON.parse(localStorageRef) });
		}
		
		this.ref = base.syncState(`${params.storeId}/fishesss`, {
			context: this,
			state: 'fishes'
		});
	}
	
	componentDidUpdate(){
	console.log('updated App state order ', this.state.order);
	localStorage.setItem(this.props.match.params.storeId, JSON.stringify(this.state.order));
	}

	componentWillUnmount() {
		console.log('dis');
		base.removeBinding(this.ref);
	}
	
	addFish = fish => {
		const fishes = { ...this.state.fishes };

 		fishes[`fish${Date.now()}`] = fish;
		// console.log(fishes);

		this.setState({
			// fishes:fishes
			fishes
		});
		// this.state.fishes.push(fish); // wrong

	};

	updateFish = (fish, updatedFish) => {
		const fishes = {...this.state.fishes};
		fishes[fish] = updatedFish;
		this.setState({
			fishes
		});
	}

	deleteFish = key => {
		const fishes = {...this.state.fishes};
		fishes[key] = null;
		this.setState({fishes});
	}

	loadSampleFishes = () => {
		this.setState({fishes:samleFishes});
	};

	addToOrder = (key) => { 
		console.log('aaddToOrder aa');
		const order = {...this.state.order};
		order[key] = order[key] + 1 || 1;
		this.setState ({
			order
		});
	}

	removeFromOrder = (key) => { 
		const order = {...this.state.order};
		delete order[key];
		this.setState ({
			order
		});
	}

	render(){
		return ( 
			<div className="catch-of-the-day">
				<div className="menu">
					<Header aa="1111" tagline={"it is woking"} age={44}/>
					<ul className="fishes">
						{/* <Fish /> */}
						{Object.keys(this.state.fishes).map(key => <Fish key={key} index={key}
							details={this.state.fishes[key]} addToOrder={this.addToOrder}
						/>)}
					</ul>	
				</div>
				<Order {...this.state} removeFromOrder={this.removeFromOrder}/> {/*fishes and order*/}
				{/* <Order fish={this.state.fishes} order={this.state.order}/> */}
				<Inventory addFish={this.addFish} loadSampleFishes={this.loadSampleFishes}
					fishes={this.state.fishes} updateFish={this.updateFish} 
					deleteFish={this.deleteFish} storeId={this.props.match.params.storeId}
				/>
			</div>
		);	
	}
}
export default App;