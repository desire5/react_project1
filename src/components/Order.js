import React from 'react';
import {formatPrice} from "../helpers";
import {TransitionGroup, CSSTransition} from "react-transition-group";

class Order extends React.Component {
	renderOrder = key => {
		const fish = this.props.fishes[key];
		const count = this.props.order[key];
		const isAvailable2 = fish && fish.status === "available";

		// make sure the fish is loaded before we contionue!
		if(!fish) return null;

		if(!isAvailable2){
			return <li key={key}>Sorry {fish ? fish.name : "fish"} is no longer available </li>;
		}
		return (
			<CSSTransition 
				classNames="order"
				key={key} 
				timeout={{enter:2000, exit:2000}}
			>
				<li key={key}> 
					<span>
					<TransitionGroup component="span" className='count'>
						<CSSTransition classNames="count" key={count} timeout={{enter:2000,exit:2000}}>
							<span>{count}</span> 
						</CSSTransition>
					</TransitionGroup>
					lbs {fish.name}
						<span className='forPrice'>{formatPrice(count * fish.price)}</span>
						<button onClick={ () => this.props.removeFromOrder(key) }> &times;</button>
					</span>
				</li>
			</CSSTransition>
			)
	}
	render(){
		const orderIds = Object.keys(this.props.order);
		// const nameOrder = this.props.fishes[orderIds].price; //wrong
		const total = orderIds.reduce((prevTotal, key) => {
			const fish = this.props.fishes[key];
			console.log('fish',fish);
			const count = this.props.order[key];
					
			const isAvailable = fish && fish.status === "available";
			if(isAvailable){
				return prevTotal + count * fish.price;
			}
			return prevTotal
		},0);

		return ( 
			<div className="order-wrap">
				<h2>ORDER</h2>
				<TransitionGroup component="ul" className="order">
					{orderIds.map(this.renderOrder)}
				</TransitionGroup>

					{/* {orderIds.map(key => 
						<li key={key}>{key} {formatPrice(this.props.fishes[key].price)}</li>
					)}  */}
			
				
				{/* {nameOrder} */}
				<div className="total">
					<strong>{formatPrice(total)}</strong>
				</div>
			</div>
		);	
		
	}
}
export default Order;