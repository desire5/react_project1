import React from 'react';

class EditFishForm extends React.Component {

	handleChange = event => {
		console.log(event.currentTarget.name); //value. do if change input. price/name/status
		const updatedFish = {
			...this.props.fish,
			[event.currentTarget.name]: event.currentTarget.value
		};
		console.log(updatedFish);
		this.props.updateFish(this.props.index, updatedFish);
	}
   
	render(){
		return ( 
		<form className="fish-edit" >
			<input name="name"  onChange={this.handleChange} value={this.props.fish.name} type="text" placeholder="Name"/> 
			<input name="price"  onChange={this.handleChange} value={this.props.fish.price} type="text" placeholder="Price"/>
			<select name="status" onChange={this.handleChange} value={this.props.fish.status} >
				<option value="available">Fresh!</option>
				<option value="unavailable">Sold Out!</option>
			</select>
			<textarea name="desc" type="text" placeholder="Desc" onChange={this.handleChange} value={this.props.fish.desc}></textarea>
			<input name="image" type="text"onChange={this.handleChange}  value={this.props.fish.image} placeholder="Image"/>
			
				<button onClick={ () => this.props.deleteFish(this.props.index)}>Remove Fish</button>
		
		</form>
		);	
		
	}

}
export default EditFishForm;