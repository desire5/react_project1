import Rebase from 're-base';
import firebase from 'firebase';

const firebaseApp = firebase.initializeApp(
    {
        apiKey: "AIzaSyB2ZLJ8QwEcsgOFbV8a-NUPkt1MiNpBh8Q",
        authDomain: "catch-of-the-day-56a55.firebaseapp.com",
        databaseURL: "https://catch-of-the-day-56a55.firebaseio.com",
        projectId: "catch-of-the-day-56a55",
        storageBucket: "catch-of-the-day-56a55.appspot.com",
        messagingSenderId: "1062690226310",
        appId: "1:1062690226310:web:8c7155912222727b84d107"
      } 
);

const base = Rebase.createClass(firebaseApp.database());

export {firebaseApp};

export default base;